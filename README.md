# sna-leakage-compensation
Code and data associated with the paper [Scalar Network Analyzer Leakage Correction](doc/sna_leakage_correction.pdf).

Contents
```
data/
  raw and processed data for experiments described in the paper
doc/
  a copy of the paper
software/maxima
  wxMaxima code for derivations in appendix A
software/python
  get_trace: utility for collecting data from Rigol DSA815 Spectrum analyzer over LAN
  correct_leakage.py: implements algorithm presented in paper
```

__get_trace.py__

read a trace with 10-count avergaing from the DSA815

```
usage: get_trace.py <ip address>
```

__correct_leakage.py__

```
usage: correct_leakage.py <directory> <base_name>

example: correct_leakage.py ../../data/attenuator 70db

```

```
filename conventions:  
  DUT measurement A: <base_name>_a.dat  
  DUT measurement B: <base_name>_b.dat  
  Noise floor: noise_floor.dat  
  TG leakage: leakage.dat  
```
