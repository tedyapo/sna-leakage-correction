#!/usr/bin/env python
#
# Driver for DSA815TG over LXI (TCP/5555)

import socket
import math
import time
import sys


class DSA815:
    def __init__(self, ip_address):
        self.sock = socket.socket()
        self.sock.connect((ip_address, 5555))
        self.max_chars = 1024

    def Send(self, command):
        self.sock.sendall(command + '\n')
        time.sleep(0.1)

    def Receive(self):
        result = ''
        while True:
            result = result + self.sock.recv(self.max_chars)
            sys.stdout.flush()
            if result.find('\n') >= 0:
                return result

    def SendReceive(self, command):
        self.Send(command)
        return self.Receive()

    def Verify(self):
        id = self.SendReceive('*IDN?')
        if id[0:25] != 'Rigol Technologies,DSA815':
            raise RuntimeError('Analyzer ID Not Recognized')

    def GetTrace(self, trace_number=1):
        freq_start = float(self.SendReceive(':FREQ:STAR?'))
        freq_stop = float(self.SendReceive(':FREQ:STOP?'))
        self.Send(":FORM:TRAC:DATA:ASC")
        command = ':TRAC:DATA? TRACE%d' % trace_number
        data = self.SendReceive(command)
        hdr_len = 3 + int(data[1:2])
        data = data[hdr_len:-1]
        values = data.split(',')
        fstep = (freq_stop - freq_start) / 600.
        freq = freq_start
        array = []
        for value in values:
            val = float(value)
            array.append((int(freq), val))
            freq = freq + fstep
        return array

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.stderr.write('usage: get_trace.py <ip address>\n')
        sys.exit(1)
    else:
        dsa = DSA815(sys.argv[1])

    aver_count = 10
    dsa.Send(':TRACE1:MODE POWERAVG')
    dsa.Send(':TRACE:AVERAGE:COUNT ' + str(aver_count))
    time.sleep(1)
    n = int(dsa.SendReceive(':TRACE:AVERAGE:COUNT:CURRENT?'))
    dsa.Send(':TRACE:AVERAGE:RESET')
    while True:
        n = int(dsa.SendReceive(':TRACE:AVERAGE:COUNT:CURRENT?'))
        sys.stderr.write('n = ' + str(n) + ' averages\n')
        if n == aver_count:
            break
        time.sleep(1)

    data = dsa.GetTrace()
    for pair in data:
        (freq, value) = pair
        print freq, value
